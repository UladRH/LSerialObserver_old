using System;
using System.Collections.Generic;
using System.Text;
using LSerialObserver.Domain.Connection;

namespace LSerialObserver.Domain.Terminal
{
    public class Message
    {
        public byte[] Data;
        public MessageDirection Direction;

        public Message(byte[] data, MessageDirection direction)
        {
            Data = data;
            Direction = direction;
        }

        public string ToHex(string delimiter = " ")
        {
            return BitConverter.ToString(Data).Replace("-", delimiter);
        }

        public string ToAscii()
        {
            return Encoding.ASCII.GetString(Data);
        }
    }

    public enum MessageDirection
    {
        In,
        Out
    }

    public class SerialPortTerminal
    {
        private SerialPortConnection _connection;
        public SerialPortOptions Options;
        public List<Message> Messages { get; } = new();
        public bool IsConnected => _connection?.IsConnected ?? false;
        public event Action<SerialPortTerminal> MessageReceived;

        public void Start()
        {
            _connection = new SerialPortConnection(Options);
            _connection.Start();
            _connection.DataReceived += HandleDataReceived;
        }

        public void Stop()
        {
            _connection.DataReceived -= HandleDataReceived;
            _connection.Stop();
        }

        private void HandleDataReceived(IConnection sender, byte[] data)
        {
            Messages.Add(new Message(data, MessageDirection.In));
            MessageReceived?.Invoke(this);
        }
    }
}
