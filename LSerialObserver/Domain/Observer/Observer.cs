using System;
using LSerialObserver.Domain.Connection;
using LSerialObserver.Domain.Parser;
using Newtonsoft.Json;

namespace LSerialObserver.Domain.Observer
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Observer
    {
        public Package LatestPackage;

        public Observer(string name, IConnection connection, IParser parser)
        {
            Name = name;
            Connection = connection;
            Parser = parser;
        }

        [JsonConstructor]
        private Observer(bool isEnabled, string name, IConnection connection, IParser parser)
            : this(name, connection, parser)
        {
            if (isEnabled) Enable();
        }

        [JsonProperty] public bool IsEnabled { get; private set; }
        [JsonProperty] public string Name { get; private set; }
        [JsonProperty] public IConnection Connection { get; }
        [JsonProperty] public IParser Parser { get; }

        public event Action<Observer> Updated;

        public void Enable()
        {
            Connection.DataReceived += Parser.HandleDataReceived;
            Parser.PackageReceived += HandlePackageReceived;
            Connection.Start();
            IsEnabled = true;
        }

        public void Disable()
        {
            Connection.DataReceived -= Parser.HandleDataReceived;
            Parser.PackageReceived -= HandlePackageReceived;
            Connection.Stop();
            IsEnabled = false;
        }

        private void HandlePackageReceived(IParser parser, Package package)
        {
            LatestPackage = package;
            Updated?.Invoke(this);
        }
    }
}
