using System;
using System.Collections;
using System.Collections.Generic;
using LSerialObserver.Domain.Connection;
using LSerialObserver.Utils;
using Newtonsoft.Json;

namespace LSerialObserver.Domain.Parser
{
    public class FixedBinaryOptions
    {
        public enum EndiannessType
        {
            Little,
            Big
        }

        public enum FieldType
        {
            Integer
        }

        public EndiannessType Endianness = EndiannessType.Little;
        public List<Field> Fields = new();
        public int Length;

        public class Field
        {
            public int Length;
            public string Name;
            public int Position;
            public FieldType Type;
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class FixedBinaryParser : IParser
    {
        private List<bool> _buffer = new();

        [JsonConstructor]
        public FixedBinaryParser(FixedBinaryOptions options)
        {
            Options = options;
        }

        [JsonProperty] public FixedBinaryOptions Options { get; private set; }

        public event Action<IParser, Package> PackageReceived;

        public void HandleDataReceived(IConnection sender, byte[] data)
        {
            AppendToBuffer(data);

            while (_buffer.Count >= Options.Length) PackageReceived?.Invoke(this, ParseNextPackage());
        }

        private void AppendToBuffer(byte[] data)
        {
            var bits = new bool[data.Length * 8];
            new BitArray(data).CopyTo(bits, 0);
            _buffer.AddRange(bits);
        }

        private Package ParseNextPackage()
        {
            Package package = new();
            var bits = PopBits(Options.Length);

            foreach (var field in Options.Fields) package.Fields.Add((field.Name, ParseField(field, bits)));

            return package;
        }

        private List<bool> PopBits(int count)
        {
            if (count > _buffer.Count)
                throw new Exception($"Not enough bits in the buffer. Needs: ${count} has: ${_buffer.Count}");

            var index = _buffer.Count - count;
            var bits = _buffer.GetRange(index, count);
            _buffer.RemoveRange(index, count);
            return bits;
        }

        private object ParseField(FixedBinaryOptions.Field f, List<bool> bits)
        {
            switch (f.Type)
            {
                case FixedBinaryOptions.FieldType.Integer:
                    return BinaryParserUtils.SignedInteger(bits.GetRange(f.Position, f.Length));
                default:
                    throw new Exception($"Unknown type: {f.Type}");
            }
        }
    }
}
