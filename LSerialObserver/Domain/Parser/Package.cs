using System.Collections.Generic;

namespace LSerialObserver.Domain.Parser
{
    public class Package
    {
        public List<(string, object)> Fields = new();
    }
}
