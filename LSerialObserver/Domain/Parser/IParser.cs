using System;
using LSerialObserver.Domain.Connection;

namespace LSerialObserver.Domain.Parser
{
    public interface IParser
    {
        public void HandleDataReceived(IConnection sender, byte[] data);

        public event Action<IParser, Package> PackageReceived;
    }
}
